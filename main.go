// Fetch latest release from gitlab-org/gitlab
// Print a summary
// Import GitLab Go library
package main

import (
	"fmt"
	"log"
	"os"
	"github.com/xanzy/go-gitlab"
)

func main() {
	// Read token from environment
	token := os.Getenv("GITLAB_TOKEN")

	if token == "" {
		token = os.Getenv("CI_JOB_TOKEN")
	}

	// Create GitLab client
	git, err := gitlab.NewClient(token)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	// Get latest release
	release, _, err := git.Releases.GetLatestRelease("gitlab-org/gitlab")
	if err != nil {
		log.Fatalf("Failed to get latest release: %v", err)
	}

	// Print release tag and description
	fmt.Printf("Latest release: %s - %s\n", release.TagName, release.Description)
}